Common Issues & FAQ
==================================================

- Q: When i want to upload the token or plan a notification the shop redirects me to either the login page or the installation successful page.
    - **A**: You probably have to clear your template cache. For an exact instructions please read here_

- Q: What is Firebase
    - **A**: Firebase is a service provided by Google it has many uses but this Plugin focuses only on the "Cloud Messaging" part. With this Plugin you can send notifications to any possible Firebase endpoint like Android and IOS apps.



.. _here: ./configuration.html#clear-your-shop-and-template-cache

|
| If you encounter any strange or weird behaviour or have any more questions feel free to contact us:
| **Technical support:** florian.becker@q-two.net
| **Legal information:** info@q-two.net