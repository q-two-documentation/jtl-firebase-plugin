Installation
==================================================

Log into your JTL-Shop Backend
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#. enter your shop URL/admin in your browser (e.g. www.yourshop.de/admin)
#. enter credentials

.. image:: ./images/admin_login.png

----------------------------------------------------------------------------

Install plugin
^^^^^^^^^^^^^^^
#. go to "Plugins->Pluginverwaltung"

    .. image:: ./images/admin_dashboard.png

#. navigate to "Upload"

.. image:: ./images/plugin_upload.png

#. after clicking on "auswählen" select downloaded plugin ".zip"-file via the filesystem dialog (e.g. "Downloads/jtl-firebase.zip")

    .. image:: ./images/plugin_upload_select.png

#. confirm upload by clicking on "Hochladen"

    .. image:: ./images/plugin_upload_confirm.png

#. navigate to "Verfügbar"
#. select "Q-Two Firebase Plugin" by checking the box left to the text

    .. image:: ./images/plugin_select.png

#. click on "Installieren" at the bottom of the page

it should now be shown under the "Aktiviert" tab

----------------------------------------------------------------------------

Select a topic
^^^^^^^^^^^^^^^^

| go to the `plugins' page`_ and navigate to the "settings" tab.

.. image:: ./images/plugin_settings.png


| enter your topic name you want notifications to be dispatched to

| **!WARNING!:** if you change your topic all notifications not yet sent will have the new topic

.. _plugins' page: configuration.html#open-plugin-settings

----------------------------------------------------------------------------

Set up a Cronjob
^^^^^^^^^^^^^^^^^^^^

| The server needs to check whenever it's time to send a notification to your clients. The JTL-Shop is only active when you have visitors browsing through your website. Therefore you have to set up a Cronjob which is a reoccurring Task your server runs every x-timesteps.
| This step is **optional** but we greatly recommend doing this.
| **disadvantages of not having a Cronjob:**
#. time of notification dispatching might be off by several hours or even days depending on your site travel
#. parallel random accesses can also lead to notifications being send more than once
#. dispatching the notifications on page load might severely increase loading time

| If you dont want to set up a Cronjob you have to set "Cronjob status" in the plugins' settings to:
| "I do not have a Cronjob".
|
| For practical reasons we'll only demonstrate how you set it up with Plesk as server-management-software. If you run any other systems, please look it up in the manual or contact your systems' administrator(s).

| You can also follow this_ tutorial until you reach step 4 (german_).

#. log in to Plesk
#. in your searchbar type "scheduled tasks" or go to **Websites & Domains > Scheduled Tasks**
#. click on "Add Task"
#. select "run a PHP script"
#. then specify the path to the script relative to your virtual host directory. You can click the directory icon to quickly locate the script file. The file you have to select is located in this sort of fashion: ``jtl-shop/includes/plugins/qtwofirebase/version/100/cronjob/cronjob.php``
#. when you are asked to specify a PHP version you have to select one newer or equal to PHP 7.0.0
#. under "Run" you can select how often the script is executed. We recommend running the script about once or twice an hour for accurate notification dispatching
#. finish set up by clicking "Ok"


.. _this: https://docs.plesk.com/en-US/obsidian/customer-guide/scheduling-tasks.65207/
.. _german: https://docs.plesk.com/de-DE/obsidian/customer-guide/planen-von-aufgaben.65207/