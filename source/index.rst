.. JTL-Firebase Plugin documentation master file, created by
   sphinx-quickstart on Tue Nov 23 11:49:43 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

JTL-Firebase Plugin
=======================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   configuration
   common_issues