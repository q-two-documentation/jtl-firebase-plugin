Configuration
===============

Open plugin settings
^^^^^^^^^^^^^^^^^^^^^^^^
#. go to your stores' admin dashboard
#. select "Plugins" tab and click on "Q-Two Firebase Plugin" or click on the gears-icon in the "Pluginverwaltung"

| now you should see something like this:

.. image:: ./images/configuration_page.png

| here you can see your last send notifications and the status of your Firebase-token. Currently it should say "token not found!"

----------------------------------------------------------------------------

Clear your template cache
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. navigate to "System->Cache"

    .. image:: ./images/system_menu.png

#. click on "Gesamten Templatecache leeren" at the bottom of the page

----------------------------------------------------------------------------

Add Firebase token
^^^^^^^^^^^^^^^^^^^^^

#. get your `Firebase token`_
#. select your Firebase-token.json from your local filesystem by clicking "Browse"
#. click "submit"

.. image:: ./images/plugin_add_token.png

.. _Firebase token: https://firebase.google.com/docs/admin/setup#initialize-sdk

----------------------------------------------------------------------------

Change Token
^^^^^^^^^^^^^^
| If you want to change the Token you can just redo `uploading the token`_
.. _uploading the token: #add-firebase-token

----------------------------------------------------------------------------

Plan a notification
^^^^^^^^^^^^^^^^^^^^^

#. go to the plugins' settings and select the tab "Plan Notification"
#. enter a Title, a text-body and date-time of dispatchment
#. click "submit" to schedule the notification

----------------------------------------------------------------------------

See your schedule
^^^^^^^^^^^^^^^^^^^

| go to the plugins' settings and select the tab "Schedule". It gives you information on what and when notifications are send.

----------------------------------------------------------------------------

Delete a scheduled notification
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| go to the plugins' schedule tab (here_) and click on the "trash" icon on the right of the notification you want to delete.

.. image:: ./images/plugin_schedule.png

.. _here: #see-your-schedule

----------------------------------------------------------------------------

Sent notifications
^^^^^^^^^^^^^^^^^^^^

| go to the plugins' page and select the tab "Log". It gives you information on what and when notifications have been sent.

----------------------------------------------------------------------------

Clear Logs
^^^^^^^^^^^^

| if storage is something to your concern the plugin has the opportunity to automatically clear old **sent!** notifications. In the plugins' settings you have the option to automatically clear logs older than 1, 2 or 3 years.